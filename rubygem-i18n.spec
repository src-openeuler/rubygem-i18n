%global gem_name i18n
Name:                rubygem-%{gem_name}
Version:             1.8.11
Release:             1
Summary:             New wave Internationalization support for Ruby
License:             MIT and (BSD or Ruby)
URL:                 https://github.com/ruby-i18n/i18n
Source0:             https://rubygems.org/gems/%{gem_name}-%{version}.gem
# Since 1.8.2 tests are not shipped with the gem, but can be checked like
# git clone --no-checkout https://github.com/ruby-i18n/i18n
# cd i18n && git archive -v -o i18n-1.8.11-tests.txz v1.8.11 test
Source1:             %{gem_name}-%{version}-tests.txz
BuildRequires:       ruby ruby(release) rubygems-devel rubygem(minitest) rubygem(mocha)
BuildRequires:       rubygem(test_declarative) rubygem(concurrent-ruby) rubygem(activesupport)
BuildArch:           noarch
%description
Ruby Internationalization and localization solution.

%package doc
Summary:             Documentation for %{name}
Requires:            %{name} = %{version}-%{release}
BuildArch:           noarch
%description doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version} -b1

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
ln -s %{_builddir}/test .
sed -i -e "/require 'bundler\/setup'/ s/^/#/" test/test_helper.rb
sed -i 's/\(require .mocha\/\)setup/\1minitest/' test/test_helper.rb
LANG=C.utf8 \
find ./test/ -type f -name '*_test.rb' -exec \
  ruby -Ilib:test '{}' \;
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/MIT-LICENSE
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/README.md

%changelog
* Wed Mar 02 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 1.8.11-1
- update to 1.8.11

* Wed Aug 19 2020 tuShenmei <tushenmei@huawei.com> - 1.1.1-1
- package init
